//
//  DeeplinkCoordinator.swift
//  Application
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import UIKit
import KMPCore

class DeeplinkCoordinator: DeeplinkCoordinatorProtocol {
    weak var nav: UINavigationController?
    init(nav: UINavigationController) {
        self.nav = nav
    }
    func present(controller: UIViewController) {
        nav?.pushViewController(controller, animated: true)
    }
}
