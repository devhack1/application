//
//  DeeplinksIntegrator.swift
//  Application
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import KMPCore
import Credits

struct DeeplinksIntegrator {
    func makeDeeplinkManagers(deeplinkProvider: DeeplinkCoordinatorContainable) -> [DeeplinkManagerProtocol] {
        [CreditsDeeplinkManager(deeplinkProvider: deeplinkProvider)]
    }
}
