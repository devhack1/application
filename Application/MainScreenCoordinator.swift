//
//  MainScreenCoordinator.swift
//  Application
//
//  Created by Вильян Яумбаев on 05.06.2021.
//

import UIKit
import KMPCards
import KMPAccounts
import Credits
import MainScreen

class MainScreenCoordinator: NSObject, MainScreenActions {

    var navigationController: UINavigationController?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func openCredit(accountModel: MainAccountModelProtocol) {
        let factory = CreditFactory()
        let vc = factory.makeCredit(
            creditModel: accountModel.makeCredit(),
            accountStorage: AccountStorage())
        navigationController?.pushViewController(vc, animated: true)
    }

    func openAccount(accountModel: MainAccountModelProtocol) {
        let factory = AccountsFactory()
        let vc = factory.makeSome(account: accountModel.makeAccount())
        navigationController?.pushViewController(vc, animated: true)
    }

    func openCard(accountModel: MainAccountModelProtocol) {
        let factory = CardFactory()
        let vc = factory.makeSome(card: accountModel.makeCard())
        navigationController?.pushViewController(vc, animated: true)
    }

    func makeMainScreenDataProvider() -> MainScreenDataProvider {
        .init(
            creditsProvider: CreditsDataProviderAdapter(dataProvider: CreditsDataProviderFactory().makeProvider()),
            cardsProvider: CardsDataProviderAdapter(dataProvider: CardDataProviderFactory().makeProvider()),
            accountsProvider: AccountsDataProviderAdapter(dataProvider: AccountDataProviderFactory().makeProvider())
        )
    }
}

extension AccountStorage: AccountStorageProtocol {
}

struct CreditsDataProviderAdapter: MainScreenAccountProviderProtocol {
    let dataProvider: CreditsDataProviderProtocol
    func loadData(completion: @escaping ([MainAccountModelProtocol]) -> Void) {
        dataProvider.loadData { (models) in
            completion(models.map { $0.makeMainScreenAccountModel() })
        }
    }
}

struct CardsDataProviderAdapter: MainScreenAccountProviderProtocol {
    let dataProvider: CardsDataProviderProtocol
    func loadData(completion: @escaping ([MainAccountModelProtocol]) -> Void) {
        dataProvider.loadData { (models) in
            completion(models.map { $0.makeMainScreenAccountModel() })
        }
    }
}

struct AccountsDataProviderAdapter: MainScreenAccountProviderProtocol {
    let dataProvider: AccountsDataProviderProtocol
    func loadData(completion: @escaping ([MainAccountModelProtocol]) -> Void) {
        dataProvider.loadData { (models) in
            completion(models.map { $0.makeMainScreenAccountModel() })
        }
    }
}

extension CreditModelProtocol {
    func makeMainScreenAccountModel() -> MainAccountModelProtocol {
        MainScreenAccountModel(name: name, payment: payment, amount: amount, datePayment: datePayment)
    }
}

extension AccountModelProtocol {
    func makeMainScreenAccountModel() -> MainAccountModelProtocol {
        MainScreenAccountModel(name: name, amount: amount)
    }
}

extension CardModelProtocol {
    func makeMainScreenAccountModel() -> MainAccountModelProtocol {
        MainScreenAccountModel(image: image, name: name, amount: amount, pincode: pincode)
    }
}

extension MainAccountModelProtocol {
    func makeCredit() -> CreditModelProtocol {
        CreditModel(name: name, payment: payment, amount: amount, datePayment: datePayment)
    }
    func makeAccount() -> AccountModelProtocol {
        AccountModel(name: name, amount: amount)
    }
    func makeCard() -> CardModelProtocol {
        CardModel(image: image, name: name, amount: amount, pincode: pincode)
    }
}
