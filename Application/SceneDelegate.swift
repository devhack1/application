//
//  SceneDelegate.swift
//  Application
//
//  Created by Вильян Яумбаев on 04.06.2021.
//

import UIKit
import KMPCards
import KMPAccounts
import Credits
import MainScreen
import KMPCore

class SceneDelegate: UIResponder, UIWindowSceneDelegate, DeeplinkCoordinatorContainable {

    var window: UIWindow?

    var mainScreenCoordinator: MainScreenCoordinator?
    var deeplinkCoordinator: DeeplinkCoordinatorProtocol?
    var deepLinkManagers: [DeeplinkManagerProtocol] = []

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = makeRootScreen()
            self.window = window
            window.makeKeyAndVisible()
            deepLinkManagers = DeeplinksIntegrator().makeDeeplinkManagers(deeplinkProvider: self)
        }
    }

    open func makeRootScreen() -> UIViewController {
        let nav = UINavigationController()
        let mainScreenCoordinator = MainScreenCoordinator(navigationController: nav)
        let deeplinkCoordinator = DeeplinkCoordinator(nav: nav)
        let mainScreen = MainScreenFactory().makeMainScreen(
            dataProvider: mainScreenCoordinator.makeMainScreenDataProvider(),
            actionInteractor: mainScreenCoordinator
        )
        self.mainScreenCoordinator = mainScreenCoordinator
        self.deeplinkCoordinator = deeplinkCoordinator
        nav.setViewControllers([mainScreen], animated: false)
        return nav
    }

    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        for context in URLContexts {
            deepLinkManagers.forEach {
                if $0.canResolve(url: context.url) {
                    $0.resolve(url: context.url)
                    return
                }
            }
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

